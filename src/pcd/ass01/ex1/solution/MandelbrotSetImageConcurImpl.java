package pcd.ass01.ex1.solution;


import pcd.ass01.ex1.Complex;


public class MandelbrotSetImageConcurImpl implements MandelbrotSetImage{
    
    
    private final static double MULTIPLICATIVE_CONSTANT_RADIUS = 0.5;
    private final Worker[] workers;
 
    
    private final int w,h;
    private final int image[]; 
    private final Complex center;
    private final double delta;
    
    
    /**
     * Creating an empty Mandelbrot set image. 
     * 
     * @param w width in pixels
     * @param h height in pixels
     * @param c center of the Mandelbrot set to be represented
     * @param radius radius of the Mandelbrot set to be represented
     */
    public MandelbrotSetImageConcurImpl(final int w, final int h, final Complex c, final double radius){
        this.w = w;
        this.h = h;
        image = new int[w*h];
        center = c;
        delta = radius/(w*MULTIPLICATIVE_CONSTANT_RADIUS);

        this.workers = new Worker[Runtime.getRuntime().availableProcessors()+1];
        
     
    }
    
    
    /**
     * 
     * Compute the image with the specified level of detail
     * 
     * See https://en.wikipedia.org/wiki/Mandelbrot_set
     * 
     * @param nIterMax number of iteration representing the level of detail
     */
    
    //parallelizzare!

    @Override
    public void compute(int nIterMax){
       
        
        //numero delle aree che ho
        int nColumns = this.w / (this.workers.length*2);
        //inizializzo contatore colonne = 0
        int iColumns = 0;
         
        for (int i = 0; i < workers.length-1; i++){
            this.workers[i] = new Worker(iColumns, nColumns, h, nIterMax, image, this, w);
            workers[i].start();
            iColumns += nColumns;
        }
        
        
        
        // this = istanza che ha creato la classe (MandelbrotSetImageConcurImpl)
        workers[workers.length - 1] = new Worker(iColumns, getWidth()-iColumns, h, nIterMax, image, this, w);
        workers[workers.length - 1].start();
        
        
        for (Worker w: workers){
                try {
                    w.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }   

    }
        
    @Override
    public Complex getPoint(int x, int y) {

        return new Complex((x - w*0.5)*delta + center.re(), center.im() - (y - h*0.5)*delta); 
    }

    @Override
    public int getHeight() {
        return h;
    }

    @Override
    public int getWidth() {
        return w;
    }

    @Override
    public int[] getImage() {
        return image;
    }
    

}
