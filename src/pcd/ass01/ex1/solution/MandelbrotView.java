package pcd.ass01.ex1.solution;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import pcd.ass01.ex1.Complex;

/**
 * Simple view of a Mandelbrot Set Image
 * 
 * @author aricci
 *
 */
public class MandelbrotView extends JFrame implements MouseMotionListener {

        private MandelbrotSetImage set;
        private MandelbrotPanel canvas;
        private JScrollPane scrollPane;
        
        /* text fields reporting the position on the complex plain, given the mouse pointer */
        private JTextField posRe, posIm;
        
        public MandelbrotView(MandelbrotSetImage set, int w, int h) {
                super("Mandelbrot Viewer");
                setSize(w, h);
                this.setResizable(false);
                
                this.set = set;
                canvas = new MandelbrotPanel(set);
                canvas.setPreferredSize(new Dimension(set.getWidth(),set.getHeight()));
            scrollPane = new JScrollPane(canvas);               

            JPanel info = new JPanel();
            posRe = new JTextField(15);
            posIm = new JTextField(15);
            posRe.setEditable(false);
            posIm.setEditable(false);
            info.add(new JLabel("Re: "));
            info.add(posRe);
            info.add(new JLabel("Im: "));
            info.add(posIm);
            
            setLayout(new BorderLayout());
            add(scrollPane, BorderLayout.CENTER);               
                add(info,BorderLayout.NORTH);
                setDefaultCloseOperation(EXIT_ON_CLOSE);
                
                canvas.addMouseMotionListener(this);
        }

        /**
         * When the mouse is moved, the position on the complex plane is updated  
         */
        public void mouseMoved(MouseEvent e) {
                Complex point = set.getPoint(e.getX(),  e.getY());                      
                posRe.setText(String.format("%.10f",point.re()));
                posIm.setText(String.format("%.10f",point.im()));
        }

        @Override
        public void mouseDragged(MouseEvent e) {}

                
        class MandelbrotPanel extends JPanel {

                private MandelbrotSetImage set;
                private BufferedImage image;
                
                public MandelbrotPanel(MandelbrotSetImage set) {
                        this.set = set;
                        image = new BufferedImage(set.getWidth(), set.getHeight(), BufferedImage.TYPE_INT_RGB);
                }

                public void paintComponent(Graphics g) {
                        super.paintComponent(g);
                        Graphics2D g2 = (Graphics2D) g;
                        image.setRGB(0, 0, set.getWidth(), set.getHeight(), set.getImage(), 0, set.getWidth());
                        g2.drawImage(image, 0, 0, null);
                }

        }

}
