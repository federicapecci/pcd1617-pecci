package pcd.ass01.ex1.solution;

import pcd.ass01.ex1.Complex;


public class Worker extends Thread {
        
        private final int h, w, nIterMax, initialRow, wAbsolute;
        private final int[] image;
        private final MandelbrotSetImage setImage;

              
        public Worker(int initialRow, int width, int height, int nIterMax, int[] image, MandelbrotSetImage setImage, int wAbsolute){
            this.initialRow = initialRow;
            this.h = height;
            this.w = width;
            this.nIterMax = nIterMax; 
            this.image = image;
            this.setImage = setImage;
            this.wAbsolute = wAbsolute;
        }
        
        
        //________
        public void run() {
            int from = initialRow;
            int to = w+from;
            for (int x = from; x < to; x++ ){ //width
                for (int y = 0; y < this.h; y++){   
            
                    Complex c = setImage.getPoint(x,y);
                    double level = MandelbrotAlgorithm.computeColor(c, nIterMax);
                    int color = (int)(level*255);
                    image[y*wAbsolute+x] = color + (color << 8)+ (color << 16);
                }
               
            }
       
            
            
        }
        
        private void log(String msg){
                System.out.println("[WORKER] "+msg);
        }
        
        
}
